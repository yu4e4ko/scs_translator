# -*- coding: utf-8 -*-

'''
Created on 2 мая 2014 г.

@author: Yuri Chupyrkin
'''

from Grammar import scnArticle
from Tests import *
from Translation import *
from pyparsing import *
import wikitools as wt
import ConfigParser 
from lxml import etree
from lxml import _elementpath
import sys
import os
import traceback
import codecs
import shutil
import logging
from _codecs import encode
from datetime import *
import ast
from SCsBuilder import *

###########################################################
CONFIG_FILE = u"wiki2sc.conf"
ArticleCount = 0
ErrorCount = 0
ErrorMessage = u""
CurrentArticle = u""
  

def setCurrentArticle(artName):
    global CurrentArticle
    CurrentArticle = artName

def addArticleCount():
    global ArticleCount
    ArticleCount += 1

def addErrorCount():
    global ErrorCount
    ErrorCount += 1


def addErrorMessage(exception):
    global ErrorMessage
    ErrorMessage += u"\n########################################\nArticle name = " \
    + CurrentArticle + u"\n" \
    + u"Error:\n"+ exception + u"\n\n"


def translate(tokens, log, outputDir, mediaDir):
    try:
        scsOutputDir = outputDir
        
        if not os.path.isdir(scsOutputDir):
            os.mkdir(scsOutputDir)
        
        translator = Translator(tokens, log, mediaDir)
        translator.createFieldList()

        scsText = translator.translate()
        scsId = translator.getId()

        if u"*" in scsId:
            scsId = scsId.replace(u"*", u"")
        
        scsFile = scsOutputDir + u"/" + scsId + u".scs"
        f = codecs.open(scsFile,'w',encoding="utf8")
        f.write(scsText)
        f.close()

        
    except Exception as ex:
        log.error(ex)
        addErrorMessage(str(ex))
        addErrorCount()


def parse(article, log, outputDir, mediaDir):
    addArticleCount()
    try:
        try:
            tokens = scnArticle.parseString(article)       
        except ParseException, err:
            log.error(err.line)
            log.error(u" "*(err.column-1) + u"^")
            log.error(err)
            
            errorMessage = u"Error line: " + err.line + u"\n" + str(err)
            addErrorMessage(errorMessage)
            addErrorCount()
            
            index = article.rfind(err.line)
            article = article[:index] + u"{{SCnEnd}}"
            tokens = scnArticle.parseString(article)
        except Exception as ex:
            log.error(ex)
            addErrorMessage(str(ex))
            addErrorCount()
            
        translate(tokens, log, outputDir, mediaDir)
    except Exception, ex:
        log.error(ex)
    
    
def _initialize(conf):
    config = ConfigParser.ConfigParser()
    config.readfp(codecs.open(conf, "r", "utf8"))
    isProxy = config.getboolean('PROXY', 'proxy')
    if isProxy:
        http_proxy = config.get('PROXY', 'http_proxy')
        os.environ["http_proxy"] = http_proxy
    else:
        os.environ["http_proxy"]=""
    
    LOG_FILENAME = config.get('LOG', 'log_file')
    logging.basicConfig(filename=LOG_FILENAME,
                        level=logging.DEBUG,
                        format=u'%(asctime)s %(name)s %(levelname)s %(message)s',
                        filemode='w')
    log = logging.getLogger("SCNML2SC")
      
    log.addHandler(logging.StreamHandler(sys.stdout))
    log.info(u'Start processing ...')
    input = config.get('TRANS', 'wikiURL')
    category = config.get('TRANS', 'category')
    outputDir = config.get('TRANS', 'output')
    mediaDir = config.get('TRANS', 'media')
    allCategories = config.getboolean('TRANS', 'allCategories')
    doNotTranslateCats = config.get('TRANS', 'doNotTranslateCats')
    
    return log,input,category,outputDir, allCategories, doNotTranslateCats,mediaDir


def downloadAllFiles(site, fileOutputDir,log):
    ailimit = 500
    params = {u'action':u'query',
              u'list':u'allimages',
              u'ailimit':ailimit,
              u'aiprop':'dimensions|mime'}

    request = wt.api.APIRequest(site, params)
    result = request.query(querycontinue=True)
    images = result['query']['allimages']
    for x in images:
        log.info(u'load image: ' + x['name'])
        file = wt.wikifile.File(site,u'File:'+x['name'])
        file.download(location = os.path.join(fileOutputDir,x['name']))





def downloadFiles(site, fileOutputDir, artName,pageid,log):
    params = {u'action':u'query',u'prop':u'images',u'titles':artName}
    request = wt.api.APIRequest(site, params)
    result = request.query(querycontinue=True)
    page_cnt = result['query']['pages'][pageid]
    if 'images' in page_cnt:
        log.info(u'load images for article: ' + artName)
        for x in  page_cnt['images']:
            image_title = x['title']

            log.info(u'load image: ' + image_title)
            file = wt.wikifile.File(site,image_title)
            loc = image_title.split(':', 1)[1]
            file.download(location = os.path.join(fileOutputDir,loc))
   
            
def downloadFromCategory(log, input, category, outputDir,mediaDir,allCategories):
    site = wt.wiki.Wiki(input + u"/api.php")
    
    cat = wt.category.Category(site, u'Category:'+category)
    pages = cat.getAllMembers()

    catOutputDir = outputDir + u"/" + getTranslit(category)
    if not os.path.isdir(catOutputDir):
        os.mkdir(catOutputDir)       

    #if not allCategories:
        #downloadAllFiles(site, mediaDir,log)

    for page in pages:
        log.info(u'-'*20 + u'Begin' + u'-'*20)
        log.info(u'parsing article:' + page.title ) 
        setCurrentArticle(page.title)  
        params = {u'action':u'query',u'titles':page.title,u'export':u'exportnowrap'}        
        request = wt.api.APIRequest(site, params)
        result = request.query(querycontinue=True)
     
        intext = result['query']['export']['*']
        root = etree.fromstring(intext)
        ns = '{' + root.nsmap[None] + '}' 
        pageid = root.find(ns + 'page/' + ns + 'id').text
        txt = root.find(ns + 'page/' + ns + 'revision/' + ns + 'text')
             
        article = txt.text
        parse(article, log, catOutputDir, mediaDir)
            
        try:
            magic = 100 # <--- it's magic!
            #downloadFiles(site, catOutputDir, page.title,pageid,log)
        except  Exception as ex:
            log.error(traceback.format_exc())   
        log.info(u'-'*21 + u'End' + u'-'*21  + u'\n'*5)
    
def downloadAllCategories(log, input, outputDir, doNotTranslateCats,mediaDir):
    noTranslationList = ast.literal_eval(doNotTranslateCats)   
    site = wt.wiki.Wiki(input + u"/api.php")
    
    params = {'action':'query',
      'list' : 'allcategories'
      }
    req = wt.api.APIRequest(site, params) 
    res = req.query(querycontinue=True)

    #downloadAllFiles(site, mediaDir,log)

    categories = res["query"]["allcategories"]    
    for category in categories:
        currentCat = category[u"*"]
        if currentCat not in noTranslationList:            
            cat = wt.category.Category(site, u'Category:'+currentCat)
            pages = cat.getAllMembers()
                    
            catOutputDir = outputDir + u"/" + getTranslit(currentCat)
            if not os.path.isdir(catOutputDir):
                    os.mkdir(catOutputDir)       
                      
            for page in pages:
                log.info(u'-'*20 + u'Begin' + u'-'*20)
                log.info(u'parsing article:' + page.title )   
                setCurrentArticle(page.title)  
                params = {u'action':u'query',u'titles':page.title,u'export':u'exportnowrap'}        
                request = wt.api.APIRequest(site, params)
                result = request.query()
              
                intext = result['query']['export']['*']
                root = etree.fromstring(intext)
                ns = '{' + root.nsmap[None] + '}' 
                pageid = root.find(ns + 'page/' + ns + 'id').text
                txt = root.find(ns + 'page/' + ns + 'revision/' + ns + 'text')
                      
                article = txt.text
                parse(article, log, catOutputDir, mediaDir)
                     
                try:
                    magic = 100 # <--- it's magic!
                    #downloadFiles(site, catOutputDir, page.title,pageid,log)
                except  Exception as ex:
                    log.error(traceback.format_exc())   
                log.info(u'-'*21 + u'End' + u'-'*21  + u'\n'*5)

def writeErrorFile(log):
    try:
        fileName = "Errors.txt"
        f = codecs.open(fileName,'w',encoding="utf8")
        f.write(ErrorMessage)
        f.close()
        log.error(u"Error file writed")
    except  Exception as ex:
        log.error(traceback.format_exc())   
    
            
def download():
    log, input, category, outputDir, allCategories, doNotTranslateCats,mediaDir = _initialize(CONFIG_FILE)
    if not allCategories:
        downloadFromCategory(log, input, category, outputDir,mediaDir,allCategories)
    else:
        downloadAllCategories(log, input, outputDir, doNotTranslateCats,mediaDir)
    
    log.info(u"#######################################################################")   
    log.info(u"######################### STAT ########################################")   
    log.info(u"#######################################################################\n")    
    log.info(u"Article count = " + str(ArticleCount))    
    log.info(u"Error count = " + str(ErrorCount))
    writeErrorFile(log)
   
if __name__ == '__main__':
    download()
