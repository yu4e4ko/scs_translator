# -*- coding: utf-8 -*-
'''
Created on 25 апреля 2014 г.

@author: Yuri Chupyrkin
'''

from Grammar import scnArticle
from pyparsing import *
from Tests import *
from Translation import *


def getLineNumber(err):
    string = str(err)
    startIndex = string.find(u':') + 1
    stringPart = string[startIndex:]
    endIndex = stringPart.find(u',') + startIndex
    result = string[startIndex:endIndex]
    return result

try:
    tokens = scnArticle.parseString(test2)
    translator = Translator(tokens, "fakeLog", "fake")
    translator.createFieldList()
    translator.printAllStorage()
    
    print "----------------------------------------------------"
    scsText = translator.translate()
    print scsText
    print "----------------------------------------------------"
    print "complete!"
 
except ParseException, err:
    print err.line
    print " "*(err.column-1) + "^"
    print err
    print getLineNumber(err)
    index = test2.rfind(err.line)
    test2 = test2[:index] + u"{{SCnEnd}}"
    print "SUB_ARTICLE!!!\n", test2, "\n!!!SUB_ARTICLE!!! "
    tokens = scnArticle.parseString(test2)
    translator = Translator(tokens, "fakeLog")
    translator.createFieldList()
    translator.printAllStorage()
    
    print "----------------------------------------------------"
    scsText = translator.translate()
    print scsText
    print "----------------------------------------------------"
    print "complete!"
 
    
    
