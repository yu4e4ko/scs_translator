# -*- coding: utf-8 -*-

'''
Created on 25 апреля 2014 г.

@author: Yuri Chupyrkin
'''


from SCsBuilder import *


class SCnField:
    def __init__(self, token, level):
        self.field = token
        self.level = level
        self.innerFields = []
        
    def addInner(self, token):
        self.innerFields.append(token)
        
        
        
class FieldStorage:
    def __init__(self):
        self.fields = []
    
    def getLevel(self, token):  
        tokenLen = len(token)
        level = 0
        if tokenLen > 1:
            if token[1].isdigit():
                level = token[1]
            else:
                if token[0] == u"SCnFieldConcept":
                    level = 1
        return int(level)
    
        
    def addField(self, token):
        level = self.getLevel(token)
        
        #ignore SCnFieldCompComment
        tokenLen = len(token)
        if tokenLen > 0:
            tokenName = token[0]
            if(tokenName == u"SCnFieldCompComment"):
                return
        
        if level == 1:
            field = SCnField(token, 1)
            self.fields.append(field)
        elif level > 1:
            innerLen = len(self.fields[-1].innerFields)
            if innerLen == 0:
                if (level - 1) == self.fields[-1].level:
                    self.fields[-1].innerFields.append(SCnField(token, level))
                else:
                    print "ERROR 1!!!!"
            else:               
                notSearched = True
                currentField = self.fields[-1]
                while notSearched:
                    if (level - 1) == currentField.level:
                        currentField.innerFields.append(SCnField(token, level))
                        notSearched = False
                    innerLen = len(currentField.innerFields)
                    if innerLen > 0:
                        currentField = currentField.innerFields[-1]
                    else:
                        print "ERROR 2!!!"
                
class Translator:
    
    def __init__(self, toks, log, mediaDir):
        self.tokens = []
        for x in toks:
            for y in x:
                self.tokens.append(y)
        self.fieldStorage = FieldStorage()
        self.builder = None
        self.log = log
        self.mediaDir = mediaDir
        
        
    def createFieldList(self):
        for token in self.tokens:
            self.fieldStorage.addField(token)
            
    def printField(self, field):
        print field.field
        innerLen = len(field.innerFields) 
        if innerLen > 0:
            for inner in field.innerFields:
                self.printField(inner)

    
    def printAllStorage(self):
        for field in self.fieldStorage.fields:
            self.printField(field)
            print "*****************************************"
            
    
    
#     def translateField(self, field):
#         scsText = self.builder.build(field)
#         innerLen = len(field.innerFields) 
#         if innerLen > 0:
#             for inner in field.innerFields:
#                 self.translateField(inner)
#         return scsText
    
    
    def translate(self):
        self.builder = Builder(self.log, self.mediaDir)
        scsText = u""
        for field in self.fieldStorage.fields:
            scsText = self.builder.build(field)  
        return scsText
            
    
    def getId(self):
        return self.builder.getId()
    
    