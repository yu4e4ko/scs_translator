# -*- coding: utf-8 -*-
'''
Created on 15 апреля 2014 г.

@author: Kolb Dmitry, Yuri Chupyrkin
'''

from pyparsing import *
 
#general tokens
level = Word(nums)
V= Suppress(u"|")
B=u"{{"
E=u"}}"
 
rusalphas = u"АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдеёжзийклмнопрстуфхцчшщъыьэюя‎"
belalphs = u"ЎўІі"

#1-Ый элемент - это какой-то странный пробел. это не обычный whitespace!!!!
othersym = u" _:.,-*()<>;?!`'/+=@\\^~⋖⋗–°&—«»-∈∋\""


symbols=rusalphas + alphanums + othersym + belalphs
TXT = Word(symbols).setResultsName("TXT")


 
varB = Literal(u"⋖")
varE = Literal(u"⋗")
varB.setParseAction(replaceWith(u"\\<"))
varE.setParseAction(replaceWith(u"\\>"))
 
 
#SCnFieldConcept rule
artCon = u"SCnFieldConcept"+Suppress(V)+ Combine(OneOrMore(TXT^nestedExpr(u"[",u"]")),u" ", adjacent=False)
articleConcept = nestedExpr(B,E,artCon).setResultsName('SCnFieldConcept')
 
#link rule
conL = Optional(Suppress(OneOrMore(TXT)) + Suppress(V)) + Combine(OneOrMore(TXT),u" ", adjacent=False)
 
#conL=delimitedList(OneOrMore(Combine(TXT)),V,True)
 
link = nestedExpr(u"[[",u"]]",conL).setResultsName('link')
 
#tagOper
strOps={}
strOps[u"SCn_объединение"] = u"\\cup"
strOps[u"SCn_пересечение"] = u"\\cap"
strOps[u"SCn_существование"] = u"\\exists"
strOps[u"SCn_строгое_надмножество"] = u"\\supset"
strOps[u"SCn_строгое_подмножество"] = u"\\subset"
strOps[u"SCn_подмножество"] = u"\\subseteq"
strOps[u"SCn_надмножество"] = u"\\supseteq"
strOps[u"SCn_принадлежность"] = u"\\ni"
strOps[u"SCn_принадлежность_элемента"] = u"\\in"
strOps[u"SCn_непринадлежность"] = u"\\notin"
strOps[u"SCn_не_равно"] = u"\\neq"
strOps[u"SCn_строгое_невключение"] = u"\\not\\subset"
strOps[u"SCn_равно"] = u"="
strOps[u"SCnTextCommentBegin"]=u"<i>/*"
strOps[u"SCnTextCommentEnd"]=u"*/</i>"
 
#################################################################
  
def conversionOp(s,l,t):
    global strOps
    t = strOps[t[0]]
    return t
    
cOp = oneOf(strOps.keys()).setParseAction(conversionOp)
tagOper=nestedExpr(B,E,cOp).setResultsName("tagOper")
 
def conversionMark(s,l,t):
    if t[0][0] == u"SCnTextMain":
        return ["<b>"+t[0][1]+"</b>"]
    if t[0][0] == u"SCnTextSecond":
        return ["<i>"+t[0][1]+"</i>"] 
    if t[0][0] == u"SCnTextKeyword":
        return [t[0][1]]
    if t[0][0] == u"SCnVar":
        return ["\\<"+t[0][1]+"\\>"]  
    ### SCl templates....
    if t[0][0] == u"SCnLVar":
        return ["[SCnLVar:]"+t[0][1]+"[:SCnLVar!]"]
    if t[0][0] == u"SCn_перечисление":
        return ["[perech:]"+t[0][1]+"[:perech!]"]
    if t[0][0] == u"SCnLConcept":
        return ["[SCnLConc:]"+t[0][1]+"[:SCnLConc!]"]
    if t[0][0] == u"SCnLAttr":
        return ["[SCnLAttr:]"+t[0][1]+"[:SCnLAttr!]"]
    if t[0][0] == u"SCnLSet":
        return ["[SCnLSet:]"+t[0][1]+"[:SCnLSet!]"]
    if t[0][0] == u"SCnLOSet":
        return ["[SCnLOSet:]"+t[0][1]+"[:SCnLOSet!]"]
    if t[0][0] == u"SCnLAtomicExist":
        return ["[SCnLAtomicExist:]"+t[0][1]+"[:SCnLAtomicExist!]"]
    if t[0][0] == u"SCnLUniversal":
        return ["[SCnLUniversal:]"+t[0][1]+"[:SCnLUniversal!]"]
    if t[0][0] == u"SCnLTex":
        return ["[SCnLTex:]"+t[0][1]+"[:SCnLTex!]"]
    if t[0][0] == u"SCnLExistentialFormula":
        return ["[SCnLExistentialFormula:]"+t[0][1]+"[:SCnLExistentialFormula!]"]
    if t[0][0] == u"SCnLExistential":
        return ["[SCnLExistential:]"+t[0][1]+"[:SCnLExistential!]"]
    if t[0][0] == u"SCnLImplication":
        return ["[SCnLImplication:]"+t[0][1]+"[:SCnLImplication!]"]
    if t[0][0] == u"SCnLThen":
        return ["[SCnLThen:]"+t[0][1]+"[:SCnLThen!]"]
    if t[0][0] == u"SCnLIf":
        return ["[SCnLIf:]"+t[0][1]+"[:SCnLIf!]"]
    if t[0][0] == u"SCnLQuantifier":
        return ["[SCnLQuantifier:]"+t[0][1]+"[:SCnLQuantifier!]"]
    if t[0][0] == u"SCnLСonnective":
        return ["[SCnLСonnective:]"+t[0][1]+"[:SCnLСonnective!]"]
    if t[0][0] == u"SCnLEquivalence":
        return ["[SCnLEquivalence:]"+t[0][1]+"[:SCnLEquivalence!]"]
 
 
 
 
otherTag = nestedExpr(B,E)
breakMark = u"SCnLevel" +V+ level+Optional(V)+ Optional(otherTag^TXT)
breakMark.setParseAction(replaceWith(u"<br/>"))
 
fontMark = Group(oneOf(u"SCnTextMain SCnTextSecond \
 SCnTextKeyword SCnVar \
 SCn_перечисление") + Suppress(V) + Combine(OneOrMore(TXT^link),u" ", adjacent=False)).setParseAction(conversionMark)
 
 
cMark = fontMark^breakMark
tagMarker = nestedExpr(B,E,cMark).setResultsName("tagMarker")
 
#tag rule
tag=tagMarker^tagOper
 
#formula
def conversionFormula(s,l,t):
    #print t[0][0]
    return ["<math>"+t[0][0]+"</math>"] 
formula = nestedExpr("<math>","</math>").setResultsName("formula")
formula.setParseAction(conversionFormula)
 
unknown = u"{" + Combine(OneOrMore(TXT^link^tag^formula),u" ", adjacent=False) + SkipTo(u"}")+ u"}"
 
#scnIdtf rule
scnIdtf = Combine(OneOrMore(TXT^link^tag^formula^unknown),u" ", adjacent=False).setResultsName("scnIdtf")

 
#textIN rule
textIN = Combine(OneOrMore(TXT^link^tag^formula +SkipTo(E)),u" ", adjacent=False).setResultsName('textIN')
textINm = Combine(OneOrMore(TXT^link),u" ", adjacent=False).setResultsName('textINm')
 
#SCnFieldSpecConSyn rule
conSyn = u"SCnFieldSpecConSyn"+Suppress(V)+level+Suppress(V)+ scnIdtf
SCnFieldSpecConSyn = nestedExpr(B,E,conSyn).setResultsName('SCnFieldSpecConSyn')
 
 
'''
SCnFieldCompComment
SCnFieldSpecConMemberSet
SCnFieldSpecConPropSuperset
SCnFieldSpecConSuperset
SCnFieldSpecConPropSubset
SCnFieldSpecConSemProxim rules
'''
fieldNamesWithTIN = oneOf(u"SCnFieldSpecConPropSubset SCnFieldSpecConSemProxim " \
                          u"SCnFieldSpecConSuperset SCnFieldSpecConMemberSet " \
                          u"SCnFieldSpecConMemberEl " \
                          u"SCnFieldCompComment SCnFieldSpecConPropSuperset ")
 
 
 
 
 
 
 
###############################################################
def commaList(expr):
    return delimitedList(expr, delim=u',')
 
def semicolonList(expr):
    return delimitedList(expr, delim=u';')
 
def wrapped_in_template_opt(template, expr):
    wrapped = expr ^ (Suppress(u'{') + Suppress(u'{') + template + expr + Suppress(u'}') + Suppress(u'}'))
    return wrapped
 
template_type_opt = Suppress(u'|') + Optional(OneOrMore(Regex(r'\d+') + u'|')).suppress()
      
operation_operators = {}
operation_operators[u'SCn_объединение'] = 'union'
operation_operators[u'SCn_пересечение'] = 'intersection'       
  
operation_result_operators = {}
operation_result_operators[u'SCn_равно'] = 'equal'
operation_result_operators[u'SCn_не_равно'] = 'not_equal'
  
###############################################       
tex_text = u'SCnLTex' + Suppress(u'|') + TXT
Tex_text = nestedExpr(B,E,tex_text).setParseAction(conversionMark)
 
scnLvar = u'SCnLVar' + Suppress(u'|') + (TXT ^ link ^ Tex_text)
SCnLvar = nestedExpr(B,E,scnLvar).setParseAction(conversionMark)
 
concept = Group(u'SCnLConcept' + Suppress(V) + Combine(OneOrMore(TXT^link),u" ", adjacent=False)).setParseAction(conversionMark)
SCnLConcept = nestedExpr(B,E,concept)#
 
attribute = Group(u'SCnLAttr' + Suppress(V) + Combine(OneOrMore(SCnLvar ^ SCnLConcept),u" ", adjacent=False)).setParseAction(conversionMark)
SCnLAttr = nestedExpr(B,E,attribute)
 
math_operand = SCnLvar ^ SCnLConcept 
math_set_body = Forward()
math_set_body = Group(u'SCnLSet' + Suppress(V) + Combine(OneOrMore(Group(commaList(math_operand ))),u" ", adjacent=False)).setParseAction(conversionMark)
Math_set_body = nestedExpr(B,E,math_set_body)
 
math_set_body2 = Math_set_body
math_set_body2 = Group(u'SCnLSet' + Suppress(V) + Combine(OneOrMore(Group(commaList(math_operand ^ math_set_body2))),u", ", adjacent=False)).setParseAction(conversionMark)
Math_set_body2 = nestedExpr(B,E,math_set_body2)
 
math_orient_set_body = Group(u'SCnLOSet' + Suppress(V) + Combine(OneOrMore(Group(commaList((Optional(SCnLAttr) + math_operand) ^ Math_set_body2))),u" ", adjacent=False)).setParseAction(conversionMark)
Math_orient_set_body = nestedExpr(B,E,math_orient_set_body)
 
 
############################
operation_op = oneOf(strOps.keys()).setParseAction(conversionOp)
operation_result_op = oneOf(operation_result_operators.keys()).setParseAction(conversionOp)
math_operation = (Math_set_body2 ^ math_operand ^ Math_orient_set_body) + nestedExpr(B,E,operation_op) + (Math_set_body2 ^ math_operand ^ Math_orient_set_body) + nestedExpr(B,E,operation_result_op) + (Math_set_body2 ^ math_operand ^ Math_orient_set_body)
         
relation_op = oneOf(strOps.keys()).setParseAction(conversionOp)
math_relation =(Math_set_body2 ^ math_operand ^ Math_orient_set_body) + nestedExpr(B,E,relation_op) + (Math_set_body2 ^ math_operand ^ Math_orient_set_body)
 
atomic_exist = Group(u'SCnLAtomicExist' + Suppress(V) + Combine(OneOrMore(Group(semicolonList(Optional(tag) + (math_operation ^ math_relation)))),u" ", adjacent=False)).setParseAction(conversionMark)
SCnLAtomicExist = nestedExpr(B,E,atomic_exist)
 
 
################### SCl grammar begin ######################################
############################################################################
############################################################################
conjunction = Forward()
disjunction = Forward()
excl_disjunction = Forward()
equivalence = Forward()
Connective = Forward()
Universal_body = Forward()
Quantifier = Forward()
SCnLEquivalence = Forward()
SCnLExistential = Forward()

#########################################
equivalence_body = Group(u'SCnLEquivalence' + template_type_opt + Combine(OneOrMore(Group((SCnLExistential ^ SCnLAtomicExist ^ Quantifier ^ Connective) + Suppress(u';') + (SCnLExistential ^ SCnLAtomicExist ^ Quantifier ^ Connective))),u" ", adjacent=False)).setParseAction(conversionMark)
SCnLEquivalence = nestedExpr(B,E,equivalence_body)
# Existential definition
existentialPostfix = Group(u'SCnLExistentialFormula' + template_type_opt + Combine(OneOrMore(Group(semicolonList(SCnLAtomicExist ^ Quantifier ^ Connective ^ SCnLEquivalence)) ^ tag),u" ", adjacent=False)).setParseAction(conversionMark)
SCnLExistentialFormula = nestedExpr(B,E,existentialPostfix)
existential_body = Group(u'SCnLExistential' + template_type_opt +   Combine(OneOrMore(Group(commaList(SCnLvar)) + SCnLExistentialFormula),u" ", adjacent=False) ).setParseAction(conversionMark)
SCnLExistential = nestedExpr(B,E,existential_body)

##########################################
implicationIF = Group(u'SCnLIf' + template_type_opt +  Combine(OneOrMore(Group(SCnLExistential ^ SCnLAtomicExist ^ Quantifier ^ Connective)),u" ", adjacent=False)).setParseAction(conversionMark)
SCnLIf = nestedExpr(B,E,implicationIF)
implicationTHEN =  Group(u'SCnLThen' + template_type_opt +  Combine(OneOrMore(Group(SCnLExistential ^ SCnLAtomicExist ^ Quantifier ^ Connective)),u" ", adjacent=False)).setParseAction(conversionMark)
SCnLThen = nestedExpr(B,E,implicationTHEN)
implication_body = Group(u'SCnLImplication' + template_type_opt + Combine(OneOrMore(Group(SCnLIf + SCnLThen)),u" ", adjacent=False)).setParseAction(conversionMark)
SCnLImplication = nestedExpr(B,E,implication_body)
 
connective_template = Suppress(u'SCnLСonnective' + template_type_opt)
connective_expr = SCnLImplication ^ conjunction ^ disjunction ^ excl_disjunction ^ equivalence
Connective = wrapped_in_template_opt(connective_template, connective_expr)
 
universal_body = Group(u'SCnLUniversal' + template_type_opt +  Combine(OneOrMore(Group(commaList(SCnLvar))  + OneOrMore(SCnLAtomicExist ^ Connective)),u" ", adjacent=False)).setParseAction(conversionMark)
Universal_body = nestedExpr(B,E, universal_body) 
 
quantifier_template = Suppress(u'SCnLQuantifier' + template_type_opt)
quantifier_expr = Universal_body ^ SCnLExistential
Quantifier = wrapped_in_template_opt(quantifier_template, quantifier_expr)

########################################n
conjunction_body = Group(u'SCnLConjunction' + template_type_opt +Combine(OneOrMore(Group(SCnLExistential ^ SCnLAtomicExist ^ Quantifier ^ Connective)),u" ", adjacent=False)).setParseAction(conversionMark)
SCnLConjunction  = nestedExpr(B,E,conjunction_body)
         
disjunction_body = Group(u'SCnLDisjunction' + template_type_opt +Combine(OneOrMore(Group(SCnLExistential ^ SCnLAtomicExist ^ Quantifier ^ Connective)),u" ", adjacent=False)).setParseAction(conversionMark)
SCnLDisjunction  = nestedExpr(B,E,disjunction_body)
 
excl_disjunction_body = Group(u'SCnLExclDisjunction' + template_type_opt +Combine(OneOrMore(Group(semicolonList(SCnLExistential ^ SCnLAtomicExist ^ Quantifier ^ Connective))),u" ", adjacent=False)).setParseAction(conversionMark)
SCnLExclDisjunction  = nestedExpr(B,E,excl_disjunction_body)
 
########################################
sclBody = Combine(OneOrMore(Quantifier),u" ", adjacent=False)
SCLbody = Combine(OneOrMore(TXT^link^formula^sclBody+SkipTo(E)),u" ", adjacent=False).setResultsName('SCLbody')
 
scLFieldNames = oneOf(u"SCnLStatement")
scnLField = scLFieldNames + Suppress(V) + level + Suppress(V) + SCLbody
SCnLField = nestedExpr(B,E,scnLField).setResultsName('SCnLField')


################### SCl grammar end ########################################
############################################################################
############################################################################  
  

levelEn = level
conWithIN = fieldNamesWithTIN+Suppress(V)+level+Suppress(V)+Optional(levelEn+Suppress(V))+ textIN + Optional(Suppress(V)+ TXT)
SCnFieldWithTIN = nestedExpr(B,E,conWithIN).setResultsName('SCnFieldWithTIN')
 
#SCnFieldCompEnum rule

compEnum= u"SCnFieldCompEnum"+Suppress(V)+level+Suppress(V)+levelEn+Suppress(V)+textIN   + Optional(Suppress(V) + TXT)
SCnFieldCompEnum = nestedExpr(B,E,compEnum).setResultsName('SCnFieldCompEnum')

compEnumWithAttr= u"SCnFieldCompEnumWithAttr"+Suppress(V)+level+Suppress(V)+levelEn+Suppress(V)+ TXT + Suppress(V) + textIN   + Optional(Suppress(V) + TXT)
SCnFieldCompEnumWithAttr = nestedExpr(B,E,compEnumWithAttr).setResultsName('SCnFieldCompEnumWithAttr')

compEnumAttr= u"SCnFieldCompEnumAttr"+Suppress(V)+level+Suppress(V)+levelEn+Suppress(V)+textIN   + Optional(Suppress(V) + TXT)
SCnFieldcompEnumAttr = nestedExpr(B,E,compEnumAttr).setResultsName('SCnFieldCompEnumAttr')
 
#SCnFieldSpecConPart rule
conPart= u"SCnFieldSpecConPart"+Suppress(V)+level+Suppress(V)+ Combine(OneOrMore(TXT+SkipTo(E)))
SCnFieldSpecConPart = nestedExpr(B,E,conPart).setResultsName('SCnFieldSpecConPart')
 
#SCnFieldSpecConSemEq rule
conSemEq= u"SCnFieldSpecConSemEq"+Suppress(V)+level+Suppress(V)+scnIdtf+Optional(Suppress(V)+Literal(u"cont"))
SCnFieldSpecConSemEq = nestedExpr(B,E,conSemEq).setResultsName('SCnFieldSpecConSemEq')

compSemEq= u"SCnFieldCompSemEq"+Suppress(V)+level+Suppress(V)+scnIdtf+Optional(Suppress(V)+Literal(u"cont"))
SCnFieldCompSemEq = nestedExpr(B,E,compSemEq).setResultsName('SCnFieldCompSemEq')
 
#SCnFieldGenRoleElRel rule
genRoleElRel= u"SCnFieldGenRoleElRel"+Suppress(V)+level+Suppress(V)+textINm+Suppress(V)+textIN
SCnFieldGenRoleElRel = nestedExpr(B,E,genRoleElRel).setResultsName('SCnFieldGenRoleElRel')

 
#SCnFieldCompArt
compArt= u"SCnFieldCompArt"+Suppress(V)+level+Suppress(V)+oneOf(u'Image Flash Media')+Suppress(V)+scnIdtf
SCnFieldCompArt = nestedExpr(B,E,compArt).setResultsName('SCnFieldCompArt')


compContent = u"SCnFieldCompContent" +Suppress(V)+level+Suppress(V) + textINm + Optional(Suppress(V) + TXT)# + Optional(Suppress(V) + TXT)
SCnFieldCompContent = nestedExpr(B,E,compContent).setResultsName('SCnFieldCompContent')


compQueryAttr= u"SCnFieldCompQueryEnum"+Suppress(V)+level+Suppress(V) + textINm + Suppress(V) + textINm   + Optional(Suppress(V) + TXT)
SCnFieldCompQueryEnum = nestedExpr(B,E,compQueryAttr).setResultsName('SCnFieldCompQueryEnum')


conPrevSec= u"SCnFieldSpecConPrevSect"+Suppress(V)+level+Suppress(V) + textINm   + Optional(Suppress(V) + TXT)
SCnFieldSpecConPrevSect = nestedExpr(B,E,conPrevSec).setResultsName('SCnFieldSpecConPrevSect')


'''
SCnFieldSpecRelDomainSet
SCnFieldSpecRelDomainSuperSet
SCnFieldSpecRelDomainIntersSet
SCnFieldSpecConDef
SCnFieldSpecConUseConst
SCnFieldSpecConStateDef
SCnFieldSpecConStateUnambObjSet
SCnFieldSpecConRuleIdent
SCnFieldSpecConStat
SCnFieldSpecConExample
SCnFieldSpecConSevStat
SCnFieldSpecConExplan
SCnFieldSpecConDomainDef
SCnFieldSpecConDomain
SCnFieldSpecConRelSchema rules
'''
 
fieldNamesWithEn = oneOf(u"SCnFieldSpecRelDomainSet SCnFieldSpecRelDomainSuperSet " \
           u"SCnFieldSpecRelDomainIntersSet SCnFieldSpecConDef " \
           u"SCnFieldSpecConUseConst SCnFieldSpecConStateDef " \
           u"SCnFieldSpecConStateUnambObjSet SCnFieldSpecConRuleIdent " \
           u"SCnFieldSpecConStat SCnFieldSpecConExample " \
           u"SCnFieldSpecConSevStat SCnFieldSpecConExplan " \
           u"SCnFieldSpecConDomainDef SCnFieldSpecConDomain " \
           u"SCnFieldSpecConRelSchema SCnFieldSpecRelDomainIntersSet " \
           u"SCnFieldSpecRelDomainIntersSet SCnFieldSpecConPrivDef " \
           u"SCnFieldSpecConGlossLink SCnFieldSpecConАntipode " \
           u"SCnFieldSpecConSCgtext SCnFieldSpecConArt " \
           u"SCnFieldSpecConFormationRule SCnFieldSpecConPrototype " \
           u"SCnFieldSpecConGoal SCnFieldSpecConQuestions " \
           u"SCnFieldSpecConConfFormat SCnFieldSpecConAddres " \
           u"SCnFieldSpecConGeoPos SCnFieldSpecConAntipode " \
           u"SCnFieldSpecConExampleConcepts SCnFieldSpecConExampleConcept "
           u"SCnFieldSpecConDate SCnFieldSpecConOrdAbConf " \
           u"SCnFieldSpecConInfLetter SCnFieldSpecConApplParticip " \
           u"SCnFieldSpecConProgramCommittee SCnFieldSpecConOrgCommittee " \
           u"SCnFieldSpecConOrgOrganizers SCnFieldSpecConSupportOrg " \
           u"SCnFieldSpecConHootels SCnFieldSpecConSiteRegHootels " \
           u"SCnFieldSpecConFoodItem SCnFieldSpecConRulePubl " \
           u"SCnFieldSpecConOffInvit SCnFieldSpecConOffProgr " \
           u"SCnFieldSpecConPublArt SCnFieldSpecConConfReport " \
           u"SCnFieldSpecConConfEmail SCnFieldSpecConLectName " \
           u"SCnFieldSpecConSection SCnFieldSpecConAuthors " \
           u"SCnFieldSpecConPresentation SCnFieldSpecConArticle " \
           u"SCnFieldSpecConStandPaper SCnFieldSpecConAction " \
           u"SCnFieldSpecConIdentifier SCnFieldSpecConAuthor " \
           u"SCnFieldSpecConPatPhotos SCnFieldSpecConCollectivePhoto " \
           u"SCnFieldSpecConPlace SCnFieldSpecConParticipant " \
           u"SCnFieldSpecConSections SCnFieldSpecConKitchen " \
           u"SCnFieldSpecConLocMap SCnFieldSpecConPublications " \
           u"SCnFieldSpecConDatePubl SCnFieldSpecConPhone " \
           u"SCnFieldSpecConSite SCnFieldSpecConExampleDescription " \
           u"SCnFieldSpecConPhoto SCnFieldSpecConNewsSubj " \
           u"SCnFieldSpecConFoundationYear SCnFieldSpecConEmplee " \
           u"SCnFieldSpecConPersonsPhoto SCnFieldSpecConEventsPhoto " \
           u"SCnFieldSpecConArticleSection SCnFieldSpecConWorkTime "\
           u"SCnFieldSpecConTimeMenu SCnFieldSpecConAverageCost "\
           u"SCnFieldSpecConListSections SCnFieldSpecConWorkGroup "
           )
 
specCon = fieldNamesWithEn+Suppress(V)+level
SCnFieldWithEnum = nestedExpr(B,E,specCon).setResultsName('SCnFieldWithEnum')

beginBlock = Group(u"SCnBlockBegin" + Optional(Suppress(V) + TXT))
BeginBlock = nestedExpr(B,E, beginBlock).setResultsName("BeginBlock")

endBlock = u"SCnBlockEnd"
EndBlock = nestedExpr(B,E, endBlock).setResultsName("EndBlock")

Blocks = BeginBlock ^ EndBlock
Comment = nestedExpr(u"<!--", u"-->")
 
field = Or([
           Comment,
           SCnFieldSpecConSyn,
           SCnFieldWithTIN,
           SCnFieldSpecConPart,
           SCnFieldCompEnum,
           SCnFieldCompEnumWithAttr,
           SCnFieldcompEnumAttr,
           SCnFieldSpecConSemEq,
           SCnFieldSpecConPrevSect,
           SCnFieldCompSemEq,
           SCnFieldGenRoleElRel,
           SCnFieldCompArt,
           SCnFieldWithEnum,
           SCnLField,
           Blocks,
           SCnFieldCompContent,
           SCnFieldCompQueryEnum
           ]).setResultsName("scnField")
                                
           
#StartTag = Group(u"<" + Combine(OneOrMore(TXT ^ u"\""),u" ", adjacent=False))
StartTag = nestedExpr(u"<", u"/>")

##################### TEST ####################################
###############################################################
 
#  
# try:
#     test123 = u"""-->"""
#     test1234 = u"""{{SCnFieldCompEnum|2|0|официальная программа мероприятий OSTIS-2012|SCnFieldSpecConOffProgr}}"""             
#                
#                                                        
#     result = comment.parseString(test123)
#     print result
#     print "#################################"
#     print result.asXML()
#              
# except ParseException, err:
#     print err.line
#     print " "*(err.column-1) + "^"
#     print err
         
##################################################################

body = (Optional(BeginBlock) + articleConcept + ZeroOrMore(field)).setResultsName("body")

beginArticle = Group(OneOrMore(Suppress(u"{{SCnBegin}}") ^ Suppress(u"{{SCnArticleBegin}}")))
endArticle = Group(OneOrMore(Suppress(u"{{SCnEnd}}") ^ Suppress(u"{{SCnArticleEnd}}")))

scnArticle=Group(Optional(StartTag) + beginArticle + body + endArticle).setResultsName('scnArticle')
