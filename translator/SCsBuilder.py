# -*- coding: utf-8 -*-
'''
Created on 25 апреля 2014 г.

@author: Yuri Chupyrkin
'''

from Identifiers import *
from Runner import *
import requests


def getTranslit(text):
    symbols = (u"абвгдеёжзийклмнопрстуфхцчшщъыьэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ  -*\"‎,()é",
               u"abvgdeejzijklmnoprstufhzcssXyXeuaABVGDEEJZIJKLMNOPRSTUFHZCSSXYXEUA__XXXXXXXe")
    tr = {ord(a):ord(b) for a, b in zip(*symbols)}
    return text.translate(tr)


def getIdentifier(text):
    return identifiers.get(text.lower(),getTranslit(text))



class Builder:
    def __init__(self, log, mediaDir):
        self.scnField = None
        self.scsString = u""
        self.id = u""
        self.log = log
        self.mediaDir = mediaDir
        


    def commentImageURL(self, url):
        result = u""
        if u"http" in url:
            result += u"/*" + url + u"*/"
        return result    

    def getFileName(self, text):
        #if u"File:" in text:
        if not u"http" in text:
            mediaDir = u"../../images/"
            fileName = text
            if u"File:" in text or u"Файл" in text:
                fileName = text[5:]
                if u"‎" in fileName: #if u"‎" in fileName:‎
                    hiddenIndex = fileName.find(u'‎')
                    fileName = fileName[:hiddenIndex]
            return u"file://" + mediaDir + fileName
        else:            
            name = text[text.rfind('/') + 1:]      
            f = open(self.mediaDir + u"/" + name,'wb')
            f.write(requests.get(text).content)
            f.close()
            return self.getFileName(name)      
        return "text"


    def processDomainString(self, text):    
        result = u""
        if u" \\cup " in text:
            tokens = text.split(u" \\cup ")
            result += u"..Y (* <= nrel_subdividing: ...(* -> "
            for token in tokens:
                result += getIdentifier(token) + u";"
            result += u"; *);;*);;\n"
            return result
        elif u" \\cap " in text:
            tokens = text.split(u" \\cap ")
            result += u"..Y (* <= nrel_intersection: ...(* -> "
            for token in tokens:
                result += getIdentifier(token) + u";"
            result += u"; *);;*);;\n"
            return result
        else:
            return getIdentifier(text)
    
    
    
    #NEW translation   
    def scnEnumsBody(self, field):
        innerLen = len(field.innerFields)
        if innerLen < 1:
            return u"...;;\n"
        result = u"...(* -> "
        for inner in field.innerFields:
            result += getIdentifier(inner.field[3][0]) + u";"
        result += u"; *);;\n"
        return result
    
    
    def scnRepeatEnumBody(self, actionText, field):
        result = u""
        for inner in field.innerFields:
            result += actionText + getIdentifier(inner.field[3][0]) + u";;\n"
        return result

    
    def translatePhoto(self):
        result = "\n"
        innerField = self.scnField.innerFields[0]
        if u".png" in innerField.field[3][0] or u".jpg" in innerField.field[3][0]:
            result += self.id + u" <- rrel_key_sc_element: ..illustration_" + self.id + u"(* <- sc_illustation;; *);;\n"
            result += u"..illustration_" + self.id + u" <= nrel_translation : ..translation_illustration_" + self.id + ";;\n"
            for innerField in self.scnField.innerFields:
                result += u"..translation_illustration_" + self.id + u"-> rrel_example : \"" +  self.getFileName(innerField.field[3][0]) + u"\";;\n"  
                result += self.commentImageURL(innerField.field[3][0])
                return result
        else:
            result += self.id + u" <= nrel_photo: "
            result += self.scnEnumsBody(self.scnField)
            return result
    
    
    
    def scnSpecConSemEq(self, actionText, semEqField):
        result = actionText + u" <= nrel_translation: .." + actionText  + u"_translated;;\n"
        result += u".." + actionText + u"_translated -> rrel_example: [" + semEqField.field[2][0] + u"](*<-lang_ru;;*);;\n"
        return result
    
    
    def scnSpecConUseConst(self, actionText, constField):
        result = actionText + u" <= nrel_used_constants : "
        result += self.scnEnumsBody(constField)
        return result
    
    
    #old translation  
    def scnFieldConcept(self):
        conceptBody = self.scnField.field[1] 
        self.id = getIdentifier(conceptBody)   
        result = u""
        if u"*" in conceptBody:
            result = u"sc_node_norole_relation->" + self.id + ";;"
        elif u"_" in conceptBody:
            result = u"sc_node_role_relation->" + self.id + ";;"
        else:
            result = u"sc_node_not_relation->" + self.id + ";;"
        result += "\n"
        result += self.id + u"=>nrel_main_ru_idtf:[" + conceptBody + u"];;\n"     
        return  result
    
    
    
    #NEW translation  
    def scnSpecConSyn(self):
        result = u"\n"
        syn = self.scnField.field[2][0] 
        result += self.id + u"=> nrel_idtf:[" + syn + u"](*<-lang_ru;;*);;\n"
        return result
    
    
    #NEW translation  
    def scnSpecConPart(self):
        result = u"\n"
        result += self.id + u"<= nrel_subdividing: " + self.scnEnumsBody(self.scnField)
        return result
      
    
    #NEW translation  
    def scnSpecConPropSuperset(self):
        result = "\n"
        result += self.id + u" <= nrel_strict_inclusion: " + getIdentifier(self.scnField.field[2][0]) + ";;\n"
        return result

    #NEW translation  
    def scnSpecConPropSubset(self):
        result = "\n"
        result += self.id + u" => nrel_strict_inclusion: " + getIdentifier(self.scnField.field[2][0]) + ";;\n"
        return result

    #old translation  
    def scnRelDomainSet(self):
        result = "\n"
        result += self.id + u"<=set_domain_set:\n{\n"
        result += self.scnEnumsBody(self.scnField)
        result += u"};;\n"
        return result

    #old translation  
    def scnRelDomainSuperSet(self):
        result = "\n"
        result += self.id + u"<=set_domain_super_set:\n{\n"
        result += self.scnEnumsBody(self.scnField)
        result += u"};;\n"
        return result

    #old translation  
    def scnRelDomainIntersSet(self):
        result = "\n"
        result += self.id + u"<=set_domain_inters_set:\n{\n"
        result += self.scnEnumsBody(self.scnField)
        result += u"};;\n"
        return result


    #NEW translation  
    def scnSpecConDef(self):
        result = "\n"
        for fieldLvl2 in self.scnField.innerFields:
            result += self.id + u" <- rrel_key_sc_element : defenition_" + self.id + u" (* <- sc_definition;; *);;\n"
            result += u"defenition_" + self.id + u" => nrel_main_idtf: [" + fieldLvl2.field[3][0] + u"](*<-lang_ru;;*);;\n"
            for fieldLvl3 in fieldLvl2.innerFields:
                fieldLvl3Name = fieldLvl3.field[0]
                if fieldLvl3Name == u"SCnFieldSpecConSemEq":
                    result += self.scnSpecConSemEq(u"definition_" + self.id, fieldLvl3)
                elif fieldLvl3Name == u"SCnFieldSpecConUseConst":
                    result += self.scnSpecConUseConst(u"definition_" + self.id, fieldLvl3)
        return result
                            
    
    #NEW translation  
    def scnGenRoleElRel(self):
        result = "\n"
        result += self.id  + u" -> " + getIdentifier(self.scnField.field[2][0]) + u";;\n"
        return result
    
    
    #old translation  
    def sncSpecConStateDef(self):
        result = "\n"
        for innerFieldLvl2 in self.scnField.innerFields:
            innerFieldLvl2Id = getIdentifier(innerFieldLvl2.field[3][0])
            result += self.id + u" <= nrel_state_definition: " + innerFieldLvl2Id + u";;\n"
            result += innerFieldLvl2Id + u" <=nrel_translation: " + innerFieldLvl2Id + u"_translation;;\n"
            for innerFieldLvl3 in innerFieldLvl2.innerFields:
                if innerFieldLvl3.field[0] == u"SCnFieldSpecConSemEq":
                    result += self.scnSpecConSemEq(innerFieldLvl2Id, innerFieldLvl3)
        return result    
    

    #old translation  
    def sncSpecConStateUnambObjSet(self):
        result = "\n"
        for innerFieldLvl2 in self.scnField.innerFields:
            innerFieldLvl2Id = getIdentifier(innerFieldLvl2.field[3][0])
            result += self.id + u" <= nrel_single_assignment_statement: " + innerFieldLvl2Id + u";;\n"
            result += innerFieldLvl2Id + u" <=nrel_translation: " + innerFieldLvl2Id + u"_translation;;\n"
            for innerFieldLvl3 in innerFieldLvl2.innerFields:
                if innerFieldLvl3.field[0] == u"SCnFieldSpecConSemEq":
                    #result += innerFieldLvl2Id + u"_translation <-[" + innerFieldLvl3.field[2][0] + u"](*<-lang_ru;;*);;\n"
                    result += self.scnSpecConSemEq(innerFieldLvl2Id, innerFieldLvl3)
        return result
    
    
    #old translation  
    def scnSpecConRuleIdent(self):
        result = "\n"
        result += u"/*" + self.scnField.field[0] + u"\n"
        for rule in self.scnField.innerFields:
            ruleName = rule.field[0]
            if ruleName == u"SCnFieldCompEnum":
                result += u"\t" + ruleName + u" " + rule.field[3][0] + u"\n"
            elif ruleName == u"SCnFieldCompComment":
                result += u"\t" + ruleName + u" " + rule.field[2][0] + u"\n"
        result += u"*/"
        return result            
                    
    
    #old translation  
    def scnSpecConSevStat(self):
        result = "\n"
        for innerFieldLvl2 in self.scnField.innerFields:
            innerFieldLvl2Id = getIdentifier(innerFieldLvl2.field[3][0])
            result += self.id + u" <-rrel_key_sc_element: " + innerFieldLvl2Id + u"(*<-sc_definition;;*);;\n"                
            result += innerFieldLvl2Id + u" <=nrel_translation: " + innerFieldLvl2Id + u"_translation;;\n"
            for innerFieldLvl3 in innerFieldLvl2.innerFields:
                innerFieldLvl3Name = innerFieldLvl3.field[0]
                if innerFieldLvl3Name == u"SCnFieldSpecConSemEq":
                    #result += innerFieldLvl2Id + u"_translation <-[" + innerFieldLvl3.field[2][0] + u"](*<-lang_ru;;*);;\n"
                    result += self.scnSpecConSemEq(innerFieldLvl2Id, innerFieldLvl3)
        return result
    
    
    #NEW translation  
    def scnSpecConExample(self):
        result = "\n"
        result += self.scnRepeatEnumBody(self.id + u" -> rrel_example : ", self.scnField)
        return result
    
    
    #NEW translation  
    def scnSpecConPrototype(self):
        result = "\n"
        result += self.scnRepeatEnumBody(self.id + u" <= nrel_analogue : ", self.scnField)
        return result
    

    #old translation  
    def scnSpecConAntipode(self):
        result = "\n"
        result += self.id + u" <=nrel_antipode:\n{\n"
        result += self.scnEnumsBody(self.scnField)
        result += u"};;\n"
        return result


    #NEW translation  
    def scnSpecConRelSchema(self):
        result = "\n"
        result += self.id + u" <=nrel_relation_shema: "
        result += self.scnEnumsBody(self.scnField)
        return result

    
    #NEW translation  
    def scnSpecConMemberEl(self):
        result = "\n"
        result += self.id + u" <- " + getIdentifier(self.scnField.field[2][0]) + u";;\n"
        return result
    
    #NEW translation  
    def scnSpecConMemberSet(self):
        result = "\n"
        result += self.id + u" -> " + getIdentifier(self.scnField.field[2][0]) + u";;\n"
        for innerField in self.scnField.innerFields:
            if innerField.field[0] == u"SCnFieldSpecConMemberSet":
                result += getIdentifier(self.scnField.field[2][0]) + u" <- " + getIdentifier(innerField.field[2][0]) + u";;\n"
        return result
    
    
    #NEW translation  
    def scnSpecConExplan(self):
        result = "\n"
        for innerFieldLvl2 in self.scnField.innerFields:
            innerFieldLvl2Id = getIdentifier(innerFieldLvl2.field[3][0])
            result += self.id + u" <- rrel_key_sc_element : explanation_" + self.id + u"(* <- sc_explanation;; *);;\n"
            result += u"explanation_" + self.id + u" => nrel_main_idtf : [" + innerFieldLvl2.field[3][0] + u"](* <- lang_ru;;*);;\n"
            for innerFieldLvl3 in innerFieldLvl2.innerFields:
                if innerFieldLvl3.field[0] == u"SCnFieldSpecConSemEq":          
                    result += self.scnSpecConSemEq(u"explanation_" + self.id, innerFieldLvl3)
                elif innerFieldLvl3.field[0] == u"SCnFieldSpecConUseConst":
                    result += self.scnSpecConUseConst(u"explanation_" + self.id, innerFieldLvl3)
        return result
               
#     def scnSpecConExplan(self):
#         result = "\n"
#         for innerFieldLvl2 in self.scnField.innerFields:
#             innerFieldLvl2Id = getIdentifier(innerFieldLvl2.field[3][0])
#             result += self.id + u" <- rrel_key_sc_element : ... (* <- sc_explanation;; \n"
#             result += u"<= nrel_sc_text_translation: ... (* -> rrel_example: [" +  innerFieldLvl2.innerFields[0].field[2][0] + u"]" 
#             result += u"(* <- lang_ru;;\n"
#             for innerFieldLvl3 in innerFieldLvl2.innerFields:
#                 if innerFieldLvl3.field[0] == u"SCnFieldSpecConUseConst":
#                     result += self.scnSpecConUseConst(u"", innerFieldLvl3)
#             result += u"*);; *);; *);;\n"
#         return result 
            
        
    #NEW translation  
    def scnSpecConExampleConcepts(self):
        result = "\n"
        result += self.scnRepeatEnumBody(self.id + u" -> rrel_example : ", self.scnField)
        return result
        
      
  
    def scnSpecConDomain(self):
        result = u"\n"
        count = 1
        for innerFieldLvl2 in self.scnField.innerFields:
            if count == 1:
                result += self.id + u" <= nrel_first_domain: " + getIdentifier(innerFieldLvl2.field[3][0]) + u";;\n"
                count += 1
            elif count == 2:
                result += self.id + u" <= nrel_second_domain: " + getIdentifier(innerFieldLvl2.field[3][0]) + u";;\n"
                count += 1
            elif count == 3:
                result += self.id + u" <= nrel_third_domain: " + getIdentifier(innerFieldLvl2.field[3][0]) + u";;\n"
                count += 1
        return result
    
    
    def scnSpecConDecomp(self):
        result = "\n"
        result += self.id + u" <= nrel_section_decomposition: "
        result += self.scnEnumsBody(self.scnField)
        return result
      
    
    def scnSpecConDomainDef(self):
        result = "\n"
        result += self.id + u" <= nrel_definitional_domain : "
        result += self.processDomainString(self.scnField.innerFields[0].field[3][0])
        return result
    
    
    def scnCompArt(self):
        return self.translatePhoto()
#         result = "\n"
#         result += self.id + u" <- rrel_key_sc_element: ..illustration_" + self.id + u"(* <- sc_illustation;; *);;\n"
#         result += u"..illustration_" + self.id + u" <= nrel_translation : ..translation_illustration_" + self.id + ";;\n"
#         innerField = self.scnField.innerFields[0]
#         #innerField = self.scnField.innerFields[0]
#         for innerField in self.scnField.innerFields:
#             result += u"..translation_illustration_" + self.id + u"-> rrel_example : \"file://" +  self.getFileName(innerField.field[3][0]) + u"\";;\n"  
#             return result
    
    
    ##############################################################################
    #########################  CONF  #############################################
    ##############################################################################
    def scnSpecConAuthor(self):
        result = "\n"
        result += self.id + u" <= nrel_author:  "
        result += self.scnEnumsBody(self.scnField)
        return result
    
    
    def scnSpecConConfFormat(self):
        result = "\n"
        for innerFieldLvl2 in self.scnField.innerFields:
            result += self.id + u" <- rrel_key_sc_element : ..format (* <- sc_format;; *);;\n"
            result += u"..format <= nrel_translation : ..translation_format_" + self.id + u";;\n"
            result += u"..translation_format_" + self.id + u" => nrel_format : ["
            result += innerFieldLvl2.field[3][0] + u"](* <- lang_ru;;*);;\n"
            
            for innerFieldLvl3 in innerFieldLvl2.innerFields:
                if innerFieldLvl3.field[0] == u"SCnFieldSpecConSemEq":          
                    result += self.scnSpecConSemEq(u"format_" + self.id, innerFieldLvl3)
                elif innerFieldLvl3.field[0] == u"SCnFieldSpecConUseConst":
                    result += self.scnSpecConUseConst(u"format_" + self.id, innerFieldLvl3)
        return result
    
    
    
    def scnSpecConGoal(self):
        result = "\n"
        for innerFieldLvl2 in self.scnField.innerFields:
            result += self.id + u" <= nrel_purpose : ["
            result += innerFieldLvl2.field[3][0] + u"](* <- lang_ru;;*);;\n"
        return result
            
    
    def scnSpecConQuestions(self):
        result = "\n"
        result += self.id + u" <= nrel_questions :  "
        result += self.scnEnumsBody(self.scnField)
        return result  
    
    
    def scnSpecConAddres(self):
        result = "\n"
        for innerFieldLvl2 in self.scnField.innerFields:
            result += self.id + u" <= nrel_address : ["
            result += innerFieldLvl2.field[3][0] + u"](* <- lang_ru;;*);;\n"
        return result
    
    def scnSpecConTimeMenu(self):
        result = "\n"
        for innerFieldLvl2 in self.scnField.innerFields:
            result += self.id + u" <= nrel_time_menu : ["
            result += innerFieldLvl2.field[3][0] + u"](* <- lang_ru;;*);;\n"
        return result
    
#     def scnSpecConAverageCost(self):
#         result = "\n"
#         for innerFieldLvl2 in self.scnField.innerFields:
#             result += self.id + u" <= nrel_cost : ["
#             result += innerFieldLvl2.field[3][0] + u"](* <- lang_ru;;*);;\n"
#         return result

    def scnSpecConGeoPos(self):
        result = "\n"
        for innerFieldLvl2 in self.scnField.innerFields:
            result += self.id + u" <= nrel_location : ["
            result += innerFieldLvl2.field[3][0] + u"](* <- lang_ru;;*);;\n"
        return result
    
    
    
    def scnSpecConSynIdentifier(self):
        result = "\n"
        for innerFieldLvl2 in self.scnField.innerFields:
            result += self.id + u" <- rrel_key_sc_element : ..syn_identifier (* <- syn_identifier;; *);;\n"
            result += u"..syn_identifier <= nrel_translation : ..translation_syn_identifier_" + self.id + u";;\n"
            result += u"..translation_syn_identifier_" + self.id + u" => nrel_syn_identifier : ["
            result += innerFieldLvl2.field[3][0] + u"](* <- lang_ru;;*);;\n"       
        return result
    
    
    def scnSpecConAction(self):
        result = "\n"
        result += self.id + u" <= nrel_actions :  "
        result += self.scnEnumsBody(self.scnField)
        return result  
    
    
    def scnSpecConAnnotation(self):
        result = "\n"
        for innerFieldLvl2 in self.scnField.innerFields:
            result += self.id + u" <= nrel_summary : ["
            result += innerFieldLvl2.field[3][0] + u"](* <- lang_ru;;*);;\n"
        return result
    
    
    def scnSpecConArticle(self):
        result = "\n"
        result += self.id + u" <= nrel_articke: "
        result += self.scnEnumsBody(self.scnField)
#         for innerFieldLvl2 in self.scnField.innerFields:
#             result += self.id + u" <= nrel_articke: ["
#             result += innerFieldLvl2.field[3][0] + u"](* <- lang_ru;;*);;\n"
        return result
    
    
    def scnSpecConArticleSection(self):
        result = "\n"
        result += self.id + u" <= nrel_section :  "
        result += self.scnEnumsBody(self.scnField)
        return result 
    
    
    
    def scnSpecConAverageCost(self):
        result = "\n"
        for innerFieldLvl2 in self.scnField.innerFields:
            result += self.id + u" <= nrel_average_cost : ["
            result += innerFieldLvl2.field[3][0] + u"](* <- lang_ru;;*);;\n"
        return result
    
    
    
    def scnSpecConCollectivePhoto(self):
        result = "\n"
        result += self.id + u" <= nrel_photo : "
        result += self.scnEnumsBody(self.scnField)
        return result 
     
     
     
    def scnSpecConComment(self):
        result = "\n"
        for innerFieldLvl2 in self.scnField.innerFields:
            result += self.id + u" <= nrel_comment : ["
            result += innerFieldLvl2.field[3][0] + u"](* <- lang_ru;;*);;\n"
        return result
    
    
    def scnSpecConContent(self):
        result = u"\n"
        for innerFieldLvl2 in self.scnField.innerFields:
            result += self.id + u" <= nrel_content : \"file://" + innerFieldLvl2.field[2][0] + u"." + innerFieldLvl2.field[3] + u"\";;\n"
        return result
    
    
    def scnSpecConDatePubl(self):
        result = "\n"
        for innerFieldLvl2 in self.scnField.innerFields:
            result += self.id + u" <= nrel_date_publ : ["
            result += innerFieldLvl2.field[3][0] + u"](* <- lang_ru;;*);;\n"
        return result
    
    
    
    def scnSpecConDecompos(self):
        result = "\n"
        result += self.id + u" <= nrel_decompos : "
        result += self.scnEnumsBody(self.scnField)
        return result 
    
    
    
    def scnSpecConDescrProject(self):
        result = u"\n"
        for innerFieldLvl2 in self.scnField.innerFields:
            result += self.id + u" <= nrel_content : \"file://" + innerFieldLvl2.field[2][0] + u"." + innerFieldLvl2.field[3] + u"\";;\n"
        return result
    
    
    
    def scnSpecConEventsPhoto(self):
        result = "\n"
        result += self.id + u" <= nrel_photo : "
        result += self.scnEnumsBody(self.scnField)
        return result 
    
    
    
    def scnSpecConFoodItem(self):
        result = "\n"
        for innerFieldLvl2 in self.scnField.innerFields:
            result += self.id + u" <= nrel_food_item: ["
            result += innerFieldLvl2.field[3][0] + u"](* <- lang_ru;;*);;\n"
        return result
    
    
    def scnSpecConFoundationYear(self):
        result = "\n"
        for innerFieldLvl2 in self.scnField.innerFields:
            result += self.id + u" <= nrel_doundation_year: ["
            result += innerFieldLvl2.field[3][0] + u"](* <- lang_ru;;*);;\n"
        return result
    
    
    
    def scnSpecConHootels(self):
        result = "\n"
        result += self.id + u" <= nrel_hootels : "
        result += self.scnEnumsBody(self.scnField)
        return result 
    
    
    
    def scnSpecConInfLetter(self):
        result = "\n"
        for innerFieldLvl2 in self.scnField.innerFields:
            result += self.id + u" <= nrel_inf_letter: ["
            result += innerFieldLvl2.field[3][0] + u"](* <- lang_ru;;*);;\n"
        return result
    
    
    
    
    def scnSpecConKitchen(self):
        result = "\n"
        result += self.scnRepeatEnumBody(self.id + u" <= nrel_country: ", self.scnField)
        return result
    
    
    
    def scnSpecConLectName(self):
        result = "\n"
        for innerFieldLvl2 in self.scnField.innerFields:
            result += self.id + u" <= nrel_name_of_doclad: ["
            result += innerFieldLvl2.field[3][0] + u"](* <- lang_ru;;*);;\n"
        return result
    
    
    
    def scnSpecConListSections(self):
        result = "\n"
        result += self.id + u" <= nrel_sections : "
        result += self.scnEnumsBody(self.scnField)
        return result 
    
    
    
    def scnSpecConLocMap(self):
        result = u"\n"
        for innerFieldLvl2 in self.scnField.innerFields:
            result += self.id + u" <= nrel_content : \"file://" + innerFieldLvl2.field[2][0] + u"." + innerFieldLvl2.field[3] + u"\";;\n"
        return result
    
    
    #неверно!
    def scnSpecConNewsSubj(self):
        result = u"\n"
        for innerFieldLvl2 in self.scnField.innerFields:
            result += self.id + u" <= nrel_content : \"file://" + innerFieldLvl2.field[3][0]  + u"\";;\n"
        return result
    
    
    
    def scnSpecConOffInvit(self):
        result = "\n"
        for innerFieldLvl2 in self.scnField.innerFields:
            result += self.id + u" <- rrel_key_sc_element : ..invit_" + self.id + u"(* <- sc_invit;; *);;\n"
            result += u"..invit_" + self.id +u" <= nrel_translation : ..translation_invit_" + self.id + u";;\n"
            result += u"..translation_invit_" + self.id + u" -> rrel_example : ["
            result += innerFieldLvl2.field[3][0] + u"](* <- lang_ru;;*);;\n"       
        return result
    
    
    
    def scnSpecConOffProgr(self):
        result = "\n"
        for innerFieldLvl2 in self.scnField.innerFields:
            result += self.id + u" <- rrel_key_sc_element : ..program_" + self.id + u"(* <- sc_program;; *);;\n"
            result += u"..program_" + self.id +u" <= nrel_translation : ..translation_program_" + self.id + u";;\n"
            result += u"..translation_program_" + self.id + u" -> rrel_example : ["
            result += innerFieldLvl2.field[3][0] + u"](* <- lang_ru;;*);;\n"       
        return result
    
    
    
    def scnSpecConOrgCommittee(self):
        result = "\n"
        result += self.id + u" <= nrel_org_committee : "
        result += self.scnEnumsBody(self.scnField)
        return result 
    
    def scnSpecConOrgOrganizers(self):
        result = "\n"
        result += self.id + u" <= nrel_org_committee : "
        result += self.scnEnumsBody(self.scnField)
        return result 
    
    
    def scnSpecConPatPhoto(self):
        return self.translatePhoto()
#         result = "\n"
#         innerField = self.scnField.innerFields[0]
#         if u".png" in innerField.field[3][0] or u".jpg" in innerField.field[3][0]:
#             result += self.id + u" <- rrel_key_sc_element: ..illustration_" + self.id + u"(* <- sc_illustation;; *);;\n"
#             result += u"..illustration_" + self.id + u" <= nrel_translation : ..translation_illustration_" + self.id + ";;\n"
#             for innerField in self.scnField.innerFields:
#                 result += u"..translation_illustration_" + self.id + u"-> rrel_example : \"" +  self.getFileName(innerField.field[3][0]) + u"\";;\n"  
#                 return result
#         else:
#             result += self.id + u" <= nrel_photo: "
#             result += self.scnEnumsBody(self.scnField)
#             return result
    
    
    def scnSpecConPersonsPhoto(self):
        result = "\n"
        result += self.id + u" <= nrel_pat_photo : "
        result += self.scnEnumsBody(self.scnField)
        return result 
    
    
    def scnSpecConPhone(self):
        result = "\n"
        for innerFieldLvl2 in self.scnField.innerFields:
            result += self.id + u" <= nrel_phone: ["
            result += innerFieldLvl2.field[3][0] + u"](* <- lang_ru;;*);;\n"
        return result
    
    
    
    def scnSpecConPlace(self):
        result = "\n"
        for innerFieldLvl2 in self.scnField.innerFields:
            result += self.id + u" <= nrel_place_ostis: ["
            result += innerFieldLvl2.field[3][0] + u"](* <- lang_ru;;*);;\n"
        return result
    
    
    def scnSpecConPresentation(self):
        result = u"\n"
        for innerFieldLvl2 in self.scnField.innerFields:
            result += self.id + u" <= nrel_presentation: \"file://" +  innerFieldLvl2.field[2][0] + "\";;\n"
        return result
    
    

    def scnSpecConProgramCommittee(self):
        result = "\n"
        result += self.id + u" <= nrel_program_committee : "
        result += self.scnEnumsBody(self.scnField)
        return result
    
    
    def scnSpecConPublArt(self):
        result = "\n"
        for innerFieldLvl2 in self.scnField.innerFields:
            result += self.id + u" <= nrel_pubk_art: ["
            result += innerFieldLvl2.field[3][0] + u"](* <- lang_ru;;*);;\n"
        return result
    
    
    def scnSpecConPublPhoto(self):
        result = "\n"
        result += self.id + u" <= nrel_photo : "
        result += self.scnEnumsBody(self.scnField)
        return result 
    
    
    
    def scnSpecConPublications(self):
        result = "\n"
        result += self.id + u" <= nrel_publication : "
        result += self.scnEnumsBody(self.scnField)
#         for innerFieldLvl2 in self.scnField.innerFields:
#             result += self.id + u" <- rrel_key_sc_element : ..publication_" + self.id + u"(* <- sc_publication;; *);;\n"
#             result += u"..publication_" + self.id +u" <= nrel_translation : ..translation_publication_" + self.id + u";;\n"
#             result += u"..translation_publication_" + self.id + u" -> rrel_example : ["
#             result += innerFieldLvl2.field[3][0] + u"](* <- lang_ru;;*);;\n"       
        return result
    
    
    
    def scnSpecConRulePubl(self):
        result = u"\n"
        for innerFieldLvl2 in self.scnField.innerFields:
            result += self.id + u" <= nrel_rule_publ: \"file://" + innerFieldLvl2.field[2][0] + u"." + innerFieldLvl2.field[3] + u"\";;\n"
        return result
    
    
    def scnSpecConSection(self):
        result = "\n"
        for innerFieldLvl2 in self.scnField.innerFields:
            result += self.id + u" <= nrel_section: ["
            result += innerFieldLvl2.field[3][0] + u"](* <- lang_ru;;*);;\n"
        return result
    
    
    def scnSpecConSections(self):
        result = "\n"
        result += self.id + u" <= nrel_sections : "
        result += self.scnEnumsBody(self.scnField)
        return result
    
    
    def scnFieldSpecConSite(self):
        result = u"\n"
        for innerFieldLvl2 in self.scnField.innerFields:
            result += self.id + u" <= nrel_site: \"" + innerFieldLvl2.field[3][0] + u"\";;\n"
        return result
    
    
    def scnSpecConSiteRegHootels(self):
        result = u"\n"
        for innerFieldLvl2 in self.scnField.innerFields:
            result += self.id + u" <= nrel_hootels_site: \"file://" + innerFieldLvl2.field[2][0] + u"." + innerFieldLvl2.field[3] + u"\";;\n"
        return result
    
    
    
    def scnSpecConStandLink(self):
        result = u"\n"
        for innerFieldLvl2 in self.scnField.innerFields:
            result += self.id + u" <= nrel_standart_link: \"file://" + innerFieldLvl2.field[2][0] + u"." + innerFieldLvl2.field[3] + u"\";;\n"
        return result
    
    
    def scnSpecConStandPaper(self):
        result = "\n"
        result += self.id + u" <= nrel_stand: "
        result += self.scnEnumsBody(self.scnField)
#         for innerFieldLvl2 in self.scnField.innerFields:
#             result += self.id + u" <- rrel_key_sc_element : ..stand_" + self.id + u"(* <- sc_stand;; *);;\n"
#             result += u"..stand_" + self.id +u" <= nrel_translation : ..translation_stand_" + self.id + u";;\n"
#             result += u"..translation_stand_" + self.id + u" -> rrel_example : ["
#             result += innerFieldLvl2.field[3][0] + u"](* <- lang_ru;;*);;\n"       
        return result
    
    
    def scnSpecConSupportOrg(self):
        result = "\n"
        result += self.id + u" <= nrel_org_committee : "
        result += self.scnEnumsBody(self.scnField)
        return result
    
    
    def scnSpecConSemEqFirstLvl(self):
        result = u"\n"
        result += self.id + u"<= nrel_translation: .." + self.id + u";;\n"
        result += u".." + self.id + u" -> rrel_example: [" + self.scnField.field[2][0] + u"](*<-lang_ru*;;);;\n"
        return result
    
    
    def scnSpecConAuthors(self):
        result = "\n"
        result += self.id + u" <= nrel_author : "
        result += self.scnEnumsBody(self.scnField)
        return result
    

    
    def scnSpecConIdentifier(self):
        result = u"\n"
        innerLength = len(self.scnField.innerFields)
        if innerLength > 1:
            #result += self.id + u" <= nrel_conidentifier: [" + self.scnField.innerFields[0].field[3][0] + u"](*<-lang_ru*;;);;\n"
            #result += self.id + u" -> " + getIdentifier(self.scnField.innerFields[1].field[3][0]) + u";;\n"
            result += self.id + u" <= nrel_email: ["
            innerLength = len(self.scnField.innerFields[1].innerFields)
            if innerLength > 0:
                attr = self.scnField.innerFields[1].innerFields[0].field[3][0]
                #result += getIdentifier(self.scnField.innerFields[1].field[3][0]) + u" -> " + attr + u";;\n"
                result += attr + u"](*<-lang_en;;*);;\n"
        return result
    
    
    def scnSpecConPatPhotos(self):
        return self.translatePhoto()
#         result = "\n"
#         innerField = self.scnField.innerFields[0]
#         if u".png" in innerField.field[3][0] or u".jpg" in innerField.field[3][0]:
#             result += self.id + u" <- rrel_key_sc_element: ..illustration_" + self.id + u"(* <- sc_illustation;; *);;\n"
#             result += u"..illustration_" + self.id + u" <= nrel_translation : ..translation_illustration_" + self.id + ";;\n"
#             for innerField in self.scnField.innerFields:
#                 result += u"..translation_illustration_" + self.id + u"-> rrel_example : \"" +  self.getFileName(innerField.field[3][0]) + u"\";;\n"  
#                 return result
#         else:
#             result += self.id + u" <= nrel_photo: "
#             result += self.scnEnumsBody(self.scnField)
#             return result
        
        
    def scnFirstLevelCompEnum(self):
        result = "\n"
        result += self.id + u" <- rrel_key_sc_element: ..illustration_" + self.id + u"(* <- sc_illustation;; *);;\n"
        result += u"..illustration_" + self.id + u" <= nrel_translation : ..translation_illustration_" + self.id + ";;\n"
        result += u"..translation_illustration_" + self.id + u"-> rrel_example : \"" +  self.getFileName(self.scnField.field[3][0]) + u"\";;\n"  
        result += self.commentImageURL(self.scnField.field[3][0])
        return result
    
    ################################################################################
    
    def build(self, field):
        self.scnField = field
        tokenLen = len(self.scnField.field)
        if tokenLen > 0:
            if self.scnField.field[0] == u"SCnFieldConcept":             
                self.scsString += self.scnFieldConcept()
            elif self.scnField.field[0] == u"SCnFieldSpecConSyn":             
                self.scsString += self.scnSpecConSyn()
            elif self.scnField.field[0] == u"SCnFieldSpecConPart":             
                self.scsString += self.scnSpecConPart()
            elif self.scnField.field[0] == u"SCnFieldSpecConPropSuperset":             
                self.scsString += self.scnSpecConPropSuperset()
            elif self.scnField.field[0] == u"SCnFieldSpecConPropSubset":             
                self.scsString += self.scnSpecConPropSubset()
            #elif self.scnField.field[0] == u"SCnFieldSpecRelDomainSet":             
                #self.scsString += self.scnRelDomainSet()    
            #elif self.scnField.field[0] == u"SCnFieldSpecRelDomainSuperSet":             
                #self.scsString += self.scnRelDomainSuperSet()    
            #elif self.scnField.field[0] == u"SCnFieldSpecRelDomainIntersSet":             
                #self.scsString += self.scnRelDomainIntersSet() 
            elif self.scnField.field[0] == u"SCnFieldSpecConDef":             
                self.scsString += self.scnSpecConDef() 
            elif self.scnField.field[0] == u"SCnFieldGenRoleElRel":             
                self.scsString += self.scnGenRoleElRel() 
            #elif self.scnField.field[0] == u"SCnFieldSpecConStateDef":             
                #self.scsString += self.sncSpecConStateDef() 
            #elif self.scnField.field[0] == u"SCnFieldSpecConStateUnambObjSet":             
                #self.scsString += self.sncSpecConStateUnambObjSet() 
            #elif self.scnField.field[0] == u"SCnFieldSpecConRuleIdent":             
                #self.scsString += self.scnSpecConRuleIdent() 
            #elif self.scnField.field[0] == u"SCnFieldSpecConSevStat":             
                #self.scsString += self.scnSpecConSevStat()
            elif self.scnField.field[0] == u"SCnFieldSpecConExample":             
                self.scsString += self.scnSpecConExample()
            elif self.scnField.field[0] == u"SCnFieldSpecConPrototype":             
                self.scsString += self.scnSpecConPrototype()
            #elif self.scnField.field[0] == u"SCnFieldSpecConAntipode":             
                #self.scsString += self.scnSpecConAntipode()
            elif self.scnField.field[0] == u"SCnFieldSpecConRelSchema":             
                self.scsString += self.scnSpecConRelSchema()
            elif self.scnField.field[0] == u"SCnFieldSpecConMemberEl":             
                self.scsString += self.scnSpecConMemberEl()
            elif self.scnField.field[0] == u"SCnFieldSpecConMemberSet":             
                self.scsString += self.scnSpecConMemberSet()    
            elif self.scnField.field[0] == u"SCnFieldSpecConExplan":             
                self.scsString += self.scnSpecConExplan()    
            elif self.scnField.field[0] == u"SCnFieldSpecConExampleConcepts":             
                self.scsString += self.scnSpecConExampleConcepts() 
            elif self.scnField.field[0] == u"SCnFieldSpecConDomain":             
                self.scsString += self.scnSpecConDomain()       
            elif self.scnField.field[0] == u"SCnFieldSpecConDecomp":             
                self.scsString += self.scnSpecConDecomp()  
            elif self.scnField.field[0] == u"SCnFieldSpecConDomainDef":             
                self.scsString += self.scnSpecConDomainDef()   
            elif self.scnField.field[0] == u"SCnFieldSpecConArt":             
                self.scsString += self.scnCompArt()   
            #################### CONF #############################
            elif self.scnField.field[0] == u"SCnFieldSpecConAuthor":             
                self.scsString += self.scnSpecConAuthor() 
            elif self.scnField.field[0] == u"SCnFieldSpecConConfFormat":             
                self.scsString += self.scnSpecConConfFormat()     
            elif self.scnField.field[0] == u"SCnFieldSpecConGoal":             
                self.scsString += self.scnSpecConGoal()   
            elif self.scnField.field[0] == u"SCnFieldSpecConQuestions":             
                self.scsString += self.scnSpecConQuestions()
            elif self.scnField.field[0] == u"SCnFieldSpecConAddres":             
                self.scsString += self.scnSpecConAddres()
            elif self.scnField.field[0] == u"SCnFieldSpecConGeoPos":             
                self.scsString += self.scnSpecConGeoPos()
            elif self.scnField.field[0] == u"SCnFieldSpecConSynIdentifier":             
                self.scsString += self.scnSpecConSynIdentifier()
            elif self.scnField.field[0] == u"SCnFieldSpecConAction":             
                self.scsString += self.scnSpecConAction()
            elif self.scnField.field[0] == u"SCnFieldSpecConAnnotation":             
                self.scsString += self.scnSpecConAnnotation()
            elif self.scnField.field[0] == u"SCnFieldSpecConArticle":             
                self.scsString += self.scnSpecConArticle()
            elif self.scnField.field[0] == u"SCnFieldSpecConArticleSection":             
                self.scsString += self.scnSpecConArticleSection()
            elif self.scnField.field[0] == u"SCnFieldSpecConAverageCost":             
                self.scsString += self.scnSpecConAverageCost()
            elif self.scnField.field[0] == u"SCnFieldSpecConCollectivePhoto":             
                self.scsString += self.scnSpecConCollectivePhoto()
            elif self.scnField.field[0] == u"SCnFieldSpecConComment":             
                self.scsString += self.scnSpecConComment()
            #elif self.scnField.field[0] == u"SCnFieldSpecConContent":             
                #self.scsString += self.scnSpecConContent()    
            elif self.scnField.field[0] == u"SCnFieldSpecConDatePubl":             
                self.scsString += self.scnSpecConDatePubl()
            elif self.scnField.field[0] == u"SCnFieldSpecConDecompos":             
                self.scsString += self.scnSpecConDecompos()
            #elif  self.scnField.field[0] == u"SCnFieldSpecConDescrProject":             
                #self.scsString += self.scnSpecConDescrProject()
            elif self.scnField.field[0] == u"SCnFieldSpecConEventsPhoto":             
                self.scsString += self.scnSpecConEventsPhoto()
            elif self.scnField.field[0] == u"SCnFieldSpecConFoodItem":             
                self.scsString += self.scnSpecConFoodItem()
            elif self.scnField.field[0] == u"SCnFieldSpecConFoundationYear":             
                self.scsString += self.scnSpecConFoundationYear()  
            elif self.scnField.field[0] == u"SCnFieldSpecConInfLetter":             
                self.scsString += self.scnSpecConInfLetter()  
            elif self.scnField.field[0] == u"SCnFieldSpecConHootels":             
                self.scsString += self.scnSpecConHootels() 
            elif self.scnField.field[0] == u"SCnFieldSpecConInfLetter":             
                self.scsString += self.scnSpecConInfLetter()   
            elif self.scnField.field[0] == u"SCnFieldSpecConKitchen":             
                self.scsString += self.scnSpecConKitchen()   
            elif self.scnField.field[0] == u"SCnFieldSpecConLectName":             
                self.scsString += self.scnSpecConLectName()  
            elif self.scnField.field[0] == u"SCnFieldSpecConListSections":             
                self.scsString += self.scnSpecConListSections() 
            #elif self.scnField.field[0] == u"SCnFieldSpecConLocMap":             
                #self.scsString += self.scnSpecConLocMap()  
            #elif self.scnField.field[0] == u"SCnFieldSpecConNewsSubj":             
                #self.scsString += self.scnSpecConNewsSubj()  
            elif self.scnField.field[0] == u"SCnFieldSpecConOffInvit":             
                self.scsString += self.scnSpecConOffInvit()
            elif self.scnField.field[0] == u"SCnFieldSpecConOffProgr":             
                self.scsString += self.scnSpecConOffProgr()
            elif self.scnField.field[0] == u"SCnFieldSpecConOrgCommittee":             
                self.scsString += self.scnSpecConOrgCommittee()
            elif self.scnField.field[0] == u"SCnFieldSpecConOrgOrganizers":             
                self.scsString += self.scnSpecConOrgOrganizers()
            elif self.scnField.field[0] == u"SCnFieldSpecConPatPhoto":             
                self.scsString += self.scnSpecConPatPhoto()
            elif self.scnField.field[0] == u"SCnFieldSpecConPersonsPhoto":             
                self.scsString += self.scnSpecConPersonsPhoto()
            elif self.scnField.field[0] == u"SCnFieldSpecConPhone":             
                self.scsString += self.scnSpecConPhone()
            elif self.scnField.field[0] == u"SCnFieldSpecConPlace":             
                self.scsString += self.scnSpecConPlace()
            #elif self.scnField.field[0] == u"SCnFieldSpecConPresentation":             
                #self.scsString += self.scnSpecConPresentation()    
            elif self.scnField.field[0] == u"SCnFieldSpecConProgramCommittee":             
                self.scsString += self.scnSpecConProgramCommittee()   
            elif self.scnField.field[0] == u"SCnFieldSpecConPublArt":             
                self.scsString += self.scnSpecConPublArt()    
            elif self.scnField.field[0] == u"SCnFieldSpecConPublPhoto":             
                self.scsString += self.scnSpecConPublPhoto()     
            elif self.scnField.field[0] == u"SCnFieldSpecConPublications":             
                self.scsString += self.scnSpecConPublications()  
            #elif self.scnField.field[0] == u"SCnFieldSpecConRulePubl":             
                #self.scsString += self.scnSpecConRulePubl()  
            elif self.scnField.field[0] == u"SCnFieldSpecConSection":             
                self.scsString += self.scnSpecConSection()  
            elif self.scnField.field[0] == u"SCnFieldSpecConSections":             
                self.scsString += self.scnSpecConSections()
            #elif self.scnField.field[0] == u"SCnFieldSpecConSite":             
                #self.scsString += self.scnFieldSpecConSite()
            #elif self.scnField.field[0] == u"SCnFieldSpecConSiteRegHootels":             
                #self.scsString += self.scnSpecConSiteRegHootels()
            #elif self.scnField.field[0] == u"SCnFieldSpecConStandLink":             
                #self.scsString += self.scnSpecConStandLink()
            elif self.scnField.field[0] == u"SCnFieldSpecConStandPaper":             
                self.scsString += self.scnSpecConStandPaper()
            elif self.scnField.field[0] == u"SCnFieldSpecConSupportOrg":             
                self.scsString += self.scnSpecConSupportOrg()
            elif self.scnField.field[0] == u"SCnFieldSpecConSemEq":             
                self.scsString += self.scnSpecConSemEqFirstLvl()
            elif self.scnField.field[0] == u"SCnFieldSpecConAuthors":             
                self.scsString += self.scnSpecConAuthors() 
            elif self.scnField.field[0] == u"SCnFieldSpecConIdentifier":             
                self.scsString += self.scnSpecConIdentifier() 
            elif self.scnField.field[0] == u"SCnFieldSpecConPatPhotos":             
                self.scsString += self.scnSpecConPatPhotos() 
            elif self.scnField.field[0] == u"SCnFieldCompEnum":             
                self.scsString += self.scnFirstLevelCompEnum() 
            
            
            else:
                magic = 100
                self.log.error(self.scnField.field[0] + u" is not implemented")
        return self.scsString
    
    
    
    def getId(self):
        return self.id
            
